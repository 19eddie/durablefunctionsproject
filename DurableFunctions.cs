using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using SendGrid.Helpers.Mail;

namespace DurableFunctionsProject
{
    public static class DurableFunctions
    {
        [FunctionName("DurableFunctionOrchestrator")]
        public static async Task<dynamic> RunOrchestrator(
            [OrchestrationTrigger] IDurableOrchestrationContext context)
        {
            //richiama la funzione per prendere 100 misurazioni dal database. attende la sua risposta.
            var detections = await context.CallActivityAsync<IEnumerable<Detection>>("DurableFunctionGetDetections", null);
            //richiama la funzione per calcolare il livello medio dell'acqua, passandole le misurazioni lette con la funzione precedente.
            //attende la sua risposta.
            var mediumLevel = await context.CallActivityAsync<double>("DurableFunctionCalculateMediumLevel", detections);
            
            var statuses = new List<Task<string>>();
            foreach (var detection in detections)
            {
                //funzione per classificare le misurazioni in ALLARME, PREALLARME, NORMALE. eseguite in parallelo
                statuses.Add(context.CallActivityAsync<string>("DurableFunctionStatus", detection));
            }
            //attende il completamento di tutte le istanze della funzione per classificare le misurazioni
            await Task.WhenAll(statuses);

            //stringa che conterr� il risultato
            var result = new List<string>();

            result.Add($"Medium Water Level: {mediumLevel}cm");

            //raggruppa i diversi stati (ALARM, PREALARM, NORMAL) e conta quanti ce ne sono per ognuno
            foreach(var e in from state in statuses
                                group state by state.Result into s
                                select new { state = s.Key, count = s.Count() })
            {
                result.Add(e.ToString());
            }

            //richiama la funzione per inviare il risultato per email
            await context.CallActivityAsync("DurableFunctionSendEmail", result);

            return result;
        }

        [FunctionName("DurableFunctionSendEmail")]
        public static void SendEmail(
            [ActivityTrigger] List<string> result,
            //binding output email
            [SendGrid(ApiKey = "CustomSendGridKeyAppSettingName")] out SendGridMessage message,
            //console log
            ILogger log)
        {
            //crea l'email e la invia
            OutgoingEmail emailObject = new OutgoingEmail
            {
                To = System.Environment.GetEnvironmentVariable("emailTo", EnvironmentVariableTarget.Process),
                From = System.Environment.GetEnvironmentVariable("emailFrom", EnvironmentVariableTarget.Process),
                Subject = "Summary of the last 100 detections",
                Body = string.Join("<br>", result)
            };
            message = new SendGridMessage();
            message.AddTo(emailObject.To);
            message.AddContent("text/html", emailObject.Body);
            message.SetFrom(new EmailAddress(emailObject.From));
            message.SetSubject(emailObject.Subject);

            log.LogInformation("Object of the email: " + emailObject.Subject);
            log.LogInformation("Receiver of the email: " + emailObject.To);
        }

        [FunctionName("DurableFunctionStatus")]
        public static string GetStatus(
            [ActivityTrigger] Detection detection)
        {
            if (detection.WaterLevel < 400)
            {
                return "NORMAL";
            } else if (detection.WaterLevel < 450)
            {
                return "PREALARM";
            }
            return "ALARM";
        }

        [FunctionName("DurableFunctionGetDetections")]
        public static IEnumerable<Detection> GetTransactions(
            [ActivityTrigger] object maxRecords,
            //binding cosmos db, legge le ultime 100 misurazioni
            [CosmosDB(
                databaseName: "smartDam",
                collectionName: "rilevazioni",
                ConnectionStringSetting = "CosmosDbConnectionString",
                SqlQuery = "SELECT top 100  * FROM c")]IEnumerable<Detection> documentsOut,
            //logger
            ILogger log)
        {
            log.LogInformation($"Fetched recent documents");
            return documentsOut as IEnumerable<Detection>;
        }

        [FunctionName("DurableFunctionCalculateMediumLevel")]
        public static double CalculateMediumLevel(
            [ActivityTrigger] IEnumerable<Detection> detections,
            //logger
            ILogger log)
        {
            log.LogInformation($"calculating medium level");
            double n = 0;
            double i = 0;
            foreach (var detection in detections)
            {
                n += detection.WaterLevel;
                i++;
            }
            log.LogInformation($"number of detections: {i}");
            log.LogInformation($"medium water level: {n / i}");
            return Math.Round(n/i);
        }

        [FunctionName("DurableFunctionStarter")]
        public static async Task<HttpResponseMessage> HttpStart(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post")] HttpRequestMessage req,
            [DurableClient] IDurableOrchestrationClient starter,
            ILogger log)
        {
            // Function input comes from the request content.
            string instanceId = await starter.StartNewAsync("DurableFunctionOrchestrator", null);

            log.LogInformation($"Started orchestration with ID = '{instanceId}'.");

            return starter.CreateCheckStatusResponse(req, instanceId);
        }
    }
    public class Detection
    {
        public string Time { get; set; }
        public double WaterLevel { get; set; }
    }
    public class OutgoingEmail
    {
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }

}